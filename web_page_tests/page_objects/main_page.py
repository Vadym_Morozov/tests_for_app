from selenium.webdriver.common.by import By

from web_page_tests.page_objects.engineering_systems_page import EngineeringSystemsPage
from web_page_tests.page_objects.search_page import SearchPage
from web_page_tests.utilities.web_ui.base_page import BasePage


class MainPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __serch_input = (By.XPATH, '//input[@id="search_query_top"]')
    __serch_button = (By.CSS_SELECTOR, 'button.btn.btn-default.button-search')
    __add_to_cart_button = (By.XPATH, '//a[@class = "button ajax_add_to_cart_button btn btn-default" and @data-id-product="148"]')
    __continue_shopping_button = (By.XPATH, '//span[@class="continue btn btn-default button exclusive-medium"]')
    __ammount_of_products_in_cart = (By.CSS_SELECTOR, 'span.ajax_cart_quantity.unvisible')
    __engineering_system_button = (By.XPATH, '//a[@data-id="6"]')

    def enter_search_query(self, text):
        self._send_keys(self.__serch_input, text)
        return self

    def click_search_button(self):
        self._click(self.__serch_button)
        return self

    def search(self, text):
        self.enter_search_query(text).click_search_button()
        return SearchPage(self.__driver)

    def click_add_to_cart(self):
        self._click(self.__add_to_cart_button)
        return self

    def click_continue_shopping(self):
        self._click(self.__continue_shopping_button)
        return self

    def is_product_added_to_cart(self):
        return self._get_text(self.__ammount_of_products_in_cart)

    def move_to_engineering_system_page(self):
        self._click(self.__engineering_system_button)
        return EngineeringSystemsPage(self.__driver)
