from selenium.webdriver.common.by import By

from web_page_tests.utilities.web_ui.base_page import BasePage


class QuickOrderPage(BasePage):

    __amount_of_products_in_cart_locator = (By.XPATH, '//span[@id="summary_products_quantity"]')

    def check_text_amount(self):
        return self._get_text(self.__amount_of_products_in_cart_locator)
