from selenium.webdriver.common.by import By

from web_page_tests.utilities.web_ui.base_page import BasePage


class SearchPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __search_counter_text_locator = (By.CSS_SELECTOR, 'span.heading-counter')

    def check_text_search_counter(self):
        return self._get_text(self.__search_counter_text_locator)
