from selenium.webdriver.common.by import By

from web_page_tests.utilities.web_ui.base_page import BasePage


class EngineeringSystemsPage(BasePage):

    __engineering_system_text_locator = (By.CSS_SELECTOR, 'span.navigation_page')

    def check_text_engineering_system(self):
        return self._get_text(self.__engineering_system_text_locator)
