from selenium.webdriver.common.by import By

from web_page_tests.utilities.web_ui.base_page import BasePage


class MyOrdersPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __orders_history_text_locator = (By.XPATH, '//h1[@class="page-heading bottom-indent"]')

    def check_text_orders_history(self):
        return self._get_text(self.__orders_history_text_locator)
