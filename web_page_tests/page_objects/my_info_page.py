from selenium.webdriver.common.by import By

from web_page_tests.utilities.web_ui.base_page import BasePage


class MyInfoPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __your_personal_info_text_locator = (By.XPATH, '//h1[@class="page-subheading"]')

    def check_text_personal_info(self):
        return self._get_text(self.__your_personal_info_text_locator)
