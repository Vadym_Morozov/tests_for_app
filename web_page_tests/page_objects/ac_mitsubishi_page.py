from selenium.webdriver.common.by import By


from web_page_tests.page_objects.add_my_address_page import AddMyAddressPage
from web_page_tests.utilities.web_ui.base_page import BasePage


class MitsubishiAcPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __mitsubishi_electric_1 = (By.XPATH, '//div[@class="product-name-block"]/a[@title="Кондиционер Mitsubishi Electric MSZ-HJ25VA/MUZ-HJ25VA"]')
    __mitsubishi_electric_1_add_to_cart = (By.XPATH, '//div[@class="button-container col-xs-7 col-md-12"]//a[@data-id-product="148"]')
    __mitsubishi_electric_2 = (By.XPATH, '//div[@class="product-name-block"]/a[@title="Кондиционер Mitsubishi Electric MSZ-HJ35VA/MUZ-HJ35VA"]')
    __mitsubishi_electric_2_add_to_cart = (By.XPATH, '//div[@class="button-container col-xs-7 col-md-12"]/a[@data-id-product="186"]')
    __make_order_button = (By.XPATH, '//div[@class="button-container"]/a')
    __continue_shopping_button = (By.CSS_SELECTOR, '[class="continue btn btn-default button exclusive-medium"]')
    __mitsubishi_text_locator = (By.XPATH, '//h1[@class="page-heading product-listing"]/span[@class="cat-name"]')
    __account_button = (By.CSS_SELECTOR, 'a.account')
    __my_account_text_locator = (By.CSS_SELECTOR, 'h1.page-heading')
    __filtr_checkbox = (By.XPATH, '//*[@id="layered_id_feature_34"]')
    __add_to_compare_button = (By.XPATH, '//a[@class="add_to_compare" and @data-id-product="148"]')
    __compare_button = (By.CSS_SELECTOR, 'form.compare-form')
    __show_all_button = (By.CSS_SELECTOR, 'form.showall')
    __product_count_text_locator = (By.CSS_SELECTOR, 'div.product-count')

    def choose_ac_1(self):
        self._click(self.__mitsubishi_electric_1)
        return self

    def choose_ac_2(self):
        self._click(self.__mitsubishi_electric_2)
        return self

    def add_to_cart_ac_1(self):
        self._click(self.__mitsubishi_electric_1_add_to_cart)
        return self

    def add_to_cart_ac_2(self):
        self._click(self.__mitsubishi_electric_2_add_to_cart)
        return self

    def push_continue_shopping(self):
        self._click(self.__continue_shopping_button)
        return self

    def push_make_order(self):
        self._click(self.__make_order_button)
        return AddMyAddressPage(self.__driver)

    def check_text_mitsubishi(self):
        return self._get_text(self.__mitsubishi_text_locator)

    def is_continue_shopping_button_visible(self):
        return self._is_visible(self.__continue_shopping_button)

    def is_make_order_button_visible(self):
        return self._is_visible(self.__make_order_button)

    def click_my_account_button(self):
        self._click(self.__account_button)
        return self

    def check_text_my_account(self):
        return self._get_text(self.__my_account_text_locator)

    def click_add_to_compare(self):
        self._click(self.__add_to_compare_button)
        return self

    def is_compare_clickable(self):
        return self._is_clickable(self.__compare_button)

    def click_show_all_button(self):
        self._click(self.__show_all_button)
        return self

    def check_text_all_pages(self):
        return self._get_text(self.__product_count_text_locator)
