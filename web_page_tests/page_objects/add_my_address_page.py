from selenium.webdriver.common.by import By

from web_page_tests.page_objects.quick_order_page import QuickOrderPage
from web_page_tests.utilities.web_ui.base_page import BasePage


class AddMyAddressPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __text_delivery_address_locator = (By.CSS_SELECTOR, 'h1.page-subheading')
    __field_company = (By.CSS_SELECTOR,'#company')
    __field_address = (By.CSS_SELECTOR,'#address1')
    __field_postcode = (By.CSS_SELECTOR,'#postcode')
    __field_city = (By.CSS_SELECTOR,'#city')
    __field_phone_number = (By.CSS_SELECTOR,'#phone')
    __save_button = (By.CSS_SELECTOR,'#submitAddress')
    __choose_region = (By.CSS_SELECTOR,'#id_state')

    def check_text_delivery_address(self):
        return self._get_text(self.__text_delivery_address_locator)

    def set_company_name(self, company):
        self._send_keys(self.__field_company, company)
        return self

    def set_address(self, address):
        self._send_keys(self.__field_address, address)
        return self

    def set_postcode(self, postcode):
        self._send_keys(self.__field_postcode, postcode)
        return self

    def set_city(self, city):
        self._send_keys(self.__field_city, city)
        return self

    def set_phone(self, phone):
        self._send_keys(self.__field_phone_number, phone)
        return self

    def choose_region(self, value):
        self._select_element(self.__choose_region, value)
        return self

    def click_save(self):
        self._click(self.__save_button)

    def add_address_info(self, company: str, address: str, postcode: int, city: str, phone: int, value: str):
        self.set_company_name(company).set_address(address).set_postcode(postcode).set_city(city).set_phone(phone).\
            choose_region(value).click_save()
        return QuickOrderPage(self.__driver)
