from selenium.webdriver.common.by import By

from web_page_tests.page_objects.ac_mitsubishi_page import MitsubishiAcPage
from web_page_tests.page_objects.add_my_address_page import AddMyAddressPage
from web_page_tests.page_objects.main_page import MainPage
from web_page_tests.page_objects.my_info_page import MyInfoPage
from web_page_tests.page_objects.my_orders_page import MyOrdersPage
from web_page_tests.utilities.web_ui.base_page import BasePage


class AuthorizedPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(self.__driver)

    __logout_button = (By.CSS_SELECTOR, 'a.logout')
    __add_my_address_button = (By.CSS_SELECTOR, 'a[title="Добавить мой адрес"]')
    __my_orders_button = (By.XPATH, '//i[@class = "icon-list-ol"]/parent::*')
    __my_personal_info_button = (By.XPATH, '//i[@class = "icon-user"]/parent::*')
    __ac_button = (By.CSS_SELECTOR, '[title="Кондиционеры"]')
    __ac_mitsubishi = (By.CSS_SELECTOR, '[title="ME Бытовые"]')
    __my_account_text_locator = (By.CSS_SELECTOR, 'h1.page-heading')
    __logo_button = (By.XPATH, '//a[@title="Ziko"]')

    def is_logout_visible(self):
        return self._is_visible(self.__logout_button)

    def click_add_my_address_button(self):
        return self._click(self.__add_my_address_button)

    def click_my_orders_button(self):
        return self._click(self.__my_orders_button)

    def click_my_personal_info(self):
        return self._click(self.__my_personal_info_button)

    def move_to_address_page(self):
        self.click_add_my_address_button()
        return AddMyAddressPage(self.__driver)

    def move_to_my_orders_page(self):
        self.click_my_orders_button()
        return MyOrdersPage(self.__driver)

    def move_to_my_info_page(self):
        self.click_my_personal_info()
        return MyInfoPage(self.__driver)

    def choose_mitsubishi(self) -> MitsubishiAcPage:
        self._move_to_element(self.__ac_button)
        self._click(self.__ac_mitsubishi)
        return MitsubishiAcPage(self.__driver)

    def click_logo(self):
        self._click(self.__logo_button)

    def move_to_main_page(self):
        self.click_logo()
        return MainPage(self.__driver)
