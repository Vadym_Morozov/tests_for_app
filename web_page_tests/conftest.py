import pytest

from web_page_tests.page_objects.login_page import LoginPage
from web_page_tests.utilities.driver_factory import DriverFactory
from web_page_tests.utilities.read_configs import ReadConfig


@pytest.fixture()
def create_driver():
    chrome_driver = DriverFactory.create_driver(ReadConfig.get_browser_id())
    chrome_driver.maximize_window()
    chrome_driver.get(ReadConfig.get_base_url())
    yield chrome_driver

    chrome_driver.quit()


@pytest.fixture()
def open_login_page(create_driver):
    return LoginPage(create_driver)


@pytest.fixture()
def open_authorized_page(create_driver):
    login_page = LoginPage(create_driver)
    authorized_page = login_page.login(ReadConfig.get_user_name(), ReadConfig.get_user_password())
    return authorized_page
