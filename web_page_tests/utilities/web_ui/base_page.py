from selenium.common import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self._driver = driver
        self.__wait = WebDriverWait(self._driver, 20)

    def __wait_until_element_located(self, locator):
        return self.__wait.until(EC.presence_of_element_located(locator))

    def __wait_until_element_clickable(self, locator):
        return self.__wait.until(EC.element_to_be_clickable(locator))

    def __wait_until_element_visible(self, locator):
        return self.__wait.until(EC.visibility_of_element_located(locator))

    def __wait_until_text_visible(self, locator, text_):
        return self.__wait.until(EC.text_to_be_present_in_element(locator, text_))

    def _send_keys(self, locator, value):
        element = self.__wait_until_element_located(locator)
        element.clear()
        element.send_keys(value)

    def _click(self, locator):
        element = self.__wait_until_element_clickable(locator)
        element.click()

    def _select_element(self, locator, value):
        element = self.__wait_until_element_located(locator)
        selector = Select(element)
        selector.select_by_value(value)

    def _is_visible(self, locator):
        try:
            self.__wait_until_element_located(locator)
            return True
        except TimeoutException:
            return False

    def _is_text_visible(self, locator, text_):
        try:
            self.__wait_until_text_visible(locator, text_)
            return True
        except TimeoutException:
            return False

    def _is_clickable(self, locator):
        try:
            self.__wait_until_element_clickable(locator)
            return True
        except TimeoutException:
            return False

    def _move_to_element(self, locator):
        action = ActionChains(self._driver)
        element = self.__wait_until_element_located(locator)
        action.move_to_element(element).perform()
        return self

    def _get_text(self, locator):
        element = self.__wait_until_element_visible(locator)
        return element.text
