import pytest


@pytest.mark.smoke
def test_login_page(open_login_page):
    login_page = open_login_page
    authorized_page = login_page.login('vadymmorozov@ukr.net', '1q2w3e4r5t')
    assert authorized_page.is_logout_visible() is True, "User was not logged-in"


@pytest.mark.smoke
def test_add_address_page(open_authorized_page):
    authorized_page = open_authorized_page
    add_my_address_page = authorized_page.move_to_address_page()
    assert add_my_address_page.check_text_delivery_address() == 'ВАШИ АДРЕСА ДОСТАВКИ', "Text wasn't find"


@pytest.mark.regression
def test_my_orders_page(open_authorized_page):
    authorized_page = open_authorized_page
    my_orders_page = authorized_page.move_to_my_orders_page()
    assert my_orders_page.check_text_orders_history() == 'ИСТОРИЯ ЗАКАЗОВ', "Text wasn't find"


@pytest.mark.regression
def test_my_info_page(open_authorized_page):
    authorized_page = open_authorized_page
    my_info_page = authorized_page.move_to_my_info_page()
    assert my_info_page.check_text_personal_info() == 'ВАША ПЕРСОНАЛЬНАЯ ИНФОРМАЦИЯ', "Text wasn't find"


@pytest.mark.regression
def test_mitsubishi_ac_page(open_authorized_page):
    authorized_page = open_authorized_page
    mitsubishi_ac_page = authorized_page.choose_mitsubishi()
    assert mitsubishi_ac_page.check_text_mitsubishi() == 'ME БЫТОВЫЕ', "Text wasn't find"


@pytest.mark.smoke
def test_add_to_cart_button(open_authorized_page):
    authorized_page = open_authorized_page
    mitsubishi_ac_page = authorized_page.choose_mitsubishi()
    add_to_cart = mitsubishi_ac_page.add_to_cart_ac_1()
    assert add_to_cart.is_continue_shopping_button_visible() is True, "Button wasn't find"


@pytest.mark.regression
def test_continue_shopping_button(open_authorized_page):
    authorized_page = open_authorized_page
    mitsubishi_ac_page = authorized_page.choose_mitsubishi()
    add_to_cart = mitsubishi_ac_page.add_to_cart_ac_1().push_continue_shopping().add_to_cart_ac_2()
    assert add_to_cart.is_make_order_button_visible() is True, "Button wasn't find"


@pytest.mark.regression
def test_compare_button(open_authorized_page):
    authorized_page = open_authorized_page
    open_mitsubishi = authorized_page.choose_mitsubishi()
    add_to_compare = open_mitsubishi.click_add_to_compare()
    assert add_to_compare.is_compare_clickable() is True, "Button doesn't clickable"


@pytest.mark.regression
def test_show_all_button(open_authorized_page):
    authorized_page = open_authorized_page
    open_mitsubishi = authorized_page.choose_mitsubishi()
    click_showall = open_mitsubishi.click_show_all_button()
    assert click_showall.check_text_all_pages() == 'Показано 1 - 67 из 67 товаров', "Button doesn't works"


@pytest.mark.regression
def test_wrong_search(open_authorized_page):
    authorized_page = open_authorized_page
    open_main_page = authorized_page.move_to_main_page()
    wrong_search = open_main_page.search('dcsdcsdc')
    assert wrong_search.check_text_search_counter() == '0 результатов было найдено.', "Function works incorrect"


@pytest.mark.smoke
def test_is_product_added_to_cart(open_authorized_page):
    authorized_page = open_authorized_page
    open_main_page = authorized_page.move_to_main_page()
    add_product_to_cart = open_main_page.click_add_to_cart().click_continue_shopping()
    assert add_product_to_cart.is_product_added_to_cart == '1', "Product wasn't added to cart"


@pytest.mark.regression
def test_is_engineering_systems_button_works(open_authorized_page):
    authorized_page = open_authorized_page
    open_main_page = authorized_page.move_to_main_page()
    move_to_engineering_systems = open_main_page.move_to_engineering_system_page()
    assert move_to_engineering_systems.check_text_engineering_system() == 'Инженерные системы', "Button doesn't works"


@pytest.mark.smoke
def test_make_first_purchase(open_authorized_page):
    authorized_page = open_authorized_page
    mitsubishi_ac_page = authorized_page.choose_mitsubishi()
    add_to_cart = mitsubishi_ac_page.add_to_cart_ac_1().push_continue_shopping().add_to_cart_ac_2().push_make_order()
    add_address_info = add_to_cart.add_address_info('abc', 'mihailovskaya str., 9', 65000, 'Odessa', '+380976265462', '326')
    assert add_address_info.check_text_amount() == '2 товары', "Product wasn't added to cart"
